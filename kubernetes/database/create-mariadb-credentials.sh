#!/bin/bash
# This script reads MARIADB_env and creates equivalent kubernetes secrets.
# It needs to be capable for creating k8s secrets by reading ENV variables as well,
#   as, that is the case with CI systems.
if [ ! -f ./kubernetes/database/MARIADB_env ]; then
  echo "Could not find ENV variables file for MARIADB - ./kubernetes/database/MARIADB_env"
  exit 1
fi

echo "First delete the old secret: MARIADB-credentials"
kubectl delete secret mariadb-credentials  || true

echo "Found MARIADB_env file, creating kubernetes secret: MARIADB-credentials"
source ./kubernetes/database/MARIADB_env


kubectl create secret generic mariadb-credentials \
  --from-literal=MARIADB_ROOT_PASSWORD=${MARIADB_ROOT_PASSWORD} \
  --from-literal=MARIADB_DATABASE=${MARIADB_DATABASE} \
  --from-literal=MARIADB_USER=${MARIADB_USER} \
  --from-literal=MARIADB_PASSWORD=${MARIADB_PASSWORD}
