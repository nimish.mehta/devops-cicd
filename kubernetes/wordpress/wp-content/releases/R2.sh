#!/bin/bash

RELEASE=R2

cd /opt/bitnami/wordpress

tar -xzvf wp-content.tar.gz

# Plugins Setup

wp ai1wm list-backups --allow-root

wp plugin activate all-in-one-wp-migration --allow-root

wp plugin install /bitnami/wordpress/wp-content/plugins/all-in-one-wp-migration-unlimited-extension.zip --allow-root

wp plugin activate all-in-one-wp-migration-unlimited-extension --allow-root

yes | wp ai1wm restore $RELEASE-DevOps.wpress --allow-root


# Important

#####################################################################################
# Grant Permissions to storage folder.

chmod -R o+rwx /bitnami/wordpress/wp-content/plugins/all-in-one-wp-migration/storage
chmod -R o+rwx /opt/bitnami/wordpress/wp-content/ai1wm-backups

#END

