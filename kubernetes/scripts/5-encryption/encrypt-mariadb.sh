#!/bin/bash


# Create a Key from Console

ALIAS_NAME_KMS_KEY_ID='b212a6c5-4ef2-4dd6-b13d-580b1445e17f'


mkdir -p /root/secrets/mariadb
mkdir -p /root/secrets/wordpress

# Encryption


aws kms encrypt --region us-east-1 --key-id $ALIAS_NAME_KMS_KEY_ID  --plaintext 12345678 --output text --query CiphertextBlob | base64 --decode > /root/secrets/mariadb/mariadb-root-password.txt
aws kms encrypt --region us-east-1 --key-id $ALIAS_NAME_KMS_KEY_ID  --plaintext bitnami_wordpress --output text --query CiphertextBlob | base64 --decode > /root/secrets/mariadb/mariadb-database.txt 
aws kms encrypt --region us-east-1 --key-id $ALIAS_NAME_KMS_KEY_ID  --plaintext asim.arain --output text --query CiphertextBlob | base64 --decode > /root/secrets/mariadb/mariadb-user.txt 
aws kms encrypt --region us-east-1 --key-id $ALIAS_NAME_KMS_KEY_ID  --plaintext abc --output text --query CiphertextBlob | base64 --decode > /root/secrets/mariadb/mariadb-password.txt 



# Decryption

aws kms decrypt --region us-east-1 --ciphertext-blob fileb:///root/secrets/mariadb/mariadb-root-password.txt --output text --query Plaintext | base64 --decode
aws kms decrypt --region us-east-1 --ciphertext-blob fileb:///root/secrets/mariadb/mariadb-database.txt --output text --query Plaintext | base64 --decode
aws kms decrypt --region us-east-1 --ciphertext-blob fileb:///root/secrets/mariadb/mariadb-user.txt --output text --query Plaintext | base64 --decode
aws kms decrypt --region us-east-1 --ciphertext-blob fileb:///root/secrets/mariadb/mariadb-password.txt  --output text --query Plaintext | base64 --decode
#END


MARIADB_ROOT_PASSWORD=`aws kms decrypt --region us-east-1 --ciphertext-blob fileb:///root/secrets/mariadb/mariadb-root-password.txt --output text --query Plaintext | base64 --decode`
MARIADB_DATABASE=`aws kms decrypt --region us-east-1 --ciphertext-blob fileb:///root/secrets/mariadb/mariadb-database.txt --output text --query Plaintext | base64 --decode`
MARIADB_USER=`aws kms decrypt --region us-east-1 --ciphertext-blob fileb:///root/secrets/mariadb/mariadb-user.txt --output text --query Plaintext | base64 --decode`
MARIADB_PASSWORD=`aws kms decrypt --region us-east-1 --ciphertext-blob fileb:///root/secrets/mariadb/mariadb-password.txt  --output text --query Plaintext | base64 --decode`


echo "\n"
echo "Credentials Decrypted"

echo " This is Mariadb root password = $MARIADB_ROOT_PASSWORD "
echo " This is Mariadb database name  = $MARIADB_DATABASE "
echo " This is Mariadb User = $MARIADB_USER "
echo " This is Mariadb password = $MARIADB_PASSWORD "


