#!/bin/bash

NODE_1=`kubectl get nodes | tail -n 2 | head -n 1 | awk '{ print $1 }'`
NODE_2=`kubectl get nodes | tail -n 1 | awk '{ print $1 }'`

for i in $NODE_1
do
echo $NODE_1
echo $NODE_2

echo -e "\nLabel for NODE_1 $NODE_1 imagepull-node-1=privaterepo-for-node-1\n"
kubectl label nodes $NODE_1 imagepull-node-1=privaterepo-for-node-1

echo -e "\nLabel for NODE_2 $NODE_2 imagepull-node-2=privaterepo-for-node-2\n"
kubectl label nodes $NODE_2 imagepull-node-2=privaterepo-2-for-node-2

done

#END
