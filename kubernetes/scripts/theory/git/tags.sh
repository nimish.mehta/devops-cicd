#!/bin/bash


REPO_ROOT_DIR='/root/demo.cloudelligent.com'

apt update -y 2>&1 >/dev/null && apt install -y git 2>&1 >/dev/null
                
                    # Latest TAG

                    echo -e "\nWe are pulling the latest TAG from our Gitlab/GitHub/BitBucket repo, the TAG we created from Console\n"

                    cd $REPO_ROOT_DIR &&   git pull

                    echo -e "\nLatest TAG is mentioned below\n"

                    TAG=`git show-ref --tags | grep "$COMMIT_ID" | awk -F / '{print $NF}'`

                    echo $TAG

                    echo -e "\nLatest TAG COMMIT ID is mentioned below, navigate to your Gitlab/Gitub/BitBucket repo & check the latest TAG\n"
                  
                    COMMIT_ID=`git rev-list --tags --date-order | head -n1`

                    echo $COMMIT_ID
#END
